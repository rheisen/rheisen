use Mix.Config

# Configure your database
config :rheisen, Rheisen.Repo,
  username: "postgres",
  password: "postgres",
  database: "rheisen_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :rheisen, RheisenWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
