defmodule Rheisen.Repo.Migrations.CreateInternalUsers do
  use Ecto.Migration

  def change do
    create table(:internal_users) do
      add :first_name, :string, null: false
      add :last_name, :string, null: false
      add :username, :string, null: false
      add :password_hash, :string, null: false
      add :failed_login_attempts, :integer
      add :last_failed_login_attempt, :utc_datetime
      timestamps()
    end

  end
end
