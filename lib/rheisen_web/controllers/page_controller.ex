defmodule RheisenWeb.PageController do
  use RheisenWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def show_contact(conn, _params) do
    render(conn, "show_contact.html")
  end

  def show_about(conn, _params) do
    render(conn, "show_about.html")
  end
end
