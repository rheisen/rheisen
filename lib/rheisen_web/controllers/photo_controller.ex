defmodule RheisenWeb.PhotoController do
  use RheisenWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end