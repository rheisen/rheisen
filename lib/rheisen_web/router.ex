defmodule RheisenWeb.Router do
  use RheisenWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RheisenWeb do
    pipe_through :browser

    get "/journal", JournalController, :index
    get "/photography", PhotoController, :index
    get "/projects", ProjectController, :index
    get "/about", PageController, :show_about
    get "/contact", PageController, :show_contact
    get "/", PageController, :index
  end

  scope "/internal", RhesienWeb do
    pipe_through :browser
    
    get "/sign-in", SessionController, :new_internal_session
    delete "/sign-out", SessionController, :delete_internal_session
  end

  # Other scopes may use custom stacks.
  # scope "/api", RheisenWeb do
  #   pipe_through :api
  # end
end
