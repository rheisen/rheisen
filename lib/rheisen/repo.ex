defmodule Rheisen.Repo do
  use Ecto.Repo,
    otp_app: :rheisen,
    adapter: Ecto.Adapters.Postgres
end
