defmodule Rheisen.User.InternalUser do
  use Ecto.Schema
  import Ecto.Changeset

  schema "internal_users" do
    field :first_name, :string
    field :last_name, :string
    field :username, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :failed_login_attempts, :integer
    field :last_failed_login_attempt, :utc_datetime

    timestamps()
  end

  @doc false
  def changeset(internal_user, attrs) do
    internal_user
    |> cast(attrs, [])
    |> validate_required([:first_name, :last_name, :username, :password])
  end
end
